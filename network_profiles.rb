require 'json'
require 'winrm'

PS_SCRIPT = <<-PS_SCRIPT
$result = @{
  "date"     = Get-Date;
  "services" = Get-Service
}
$result | ConvertTo-JSON
PS_SCRIPT

url_params = {
  :ipaddress => "<hostname_or_ip>",
  :port      => 5985                # Default port 5985
}

connect_params = {
  :user         => "<username>",    # Example: domain\\user
  :pass         => "<password>",
  :disable_sspi => true
}

url = "http://#{url_params[:ipaddress]}:#{url_params[:port]}/wsman"
$evm.log("info", "Connecting to WinRM on URL :#{url}")

winrm   = WinRM::WinRMWebService.new(url, :ssl, connect_params)
results = winrm.run_powershell_script(PS_SCRIPT)

# results is a hash with 2 keys:
# 1) The first key :data is an array with two hashes
#   :stderr
#   :stdout
# 2) the second key is the :exitcode.

errors = results[:data].collect { |d| d[:stderr] }.join
$evm.log("error", "WinRM returned stderr: #{errors}") unless errors.blank?

data = results[:data].collect { |d| d[:stdout] }.join
json_hash = JSON.parse(data, :symbolize_names => true)
$evm.log("info", "WinRM returned hash: #{json_hash.inspect}")